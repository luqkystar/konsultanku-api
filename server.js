// Inisialisasi awal fastify.
const fastify = require('fastify')
({ logger: true }) //aktifkan ini untuk menerima log setiap request dari fastify.

//Fungsi ini untuk membuat kita bisa melakuakn post melalui www-url-encoded.
//fastify.register(require('fastify-formbody'));

//Route Ujicoba
fastify.get('/', function (request, reply) {
    reply.send('Hello World');
});
fastify.get('/hello', async (request, reply) => {
    return { hello: 'world' }
});

fastify.route({
    method: 'GET',
    url: '/hello/:hello/:data',
    schema: {
     params: {
      type: 'object',
      properties: {
        hello: { type: 'string' },
        data: { type: 'string' }
      }
     },
     response: {
      200: {
       type: 'object',
       properties: {
        hello: { type: 'string' },
        data: { type: 'string' },
       
       }
      }
     }
    },
    handler: async (request) => {
     const {hello, data} = request.params
     return {
    hello,
      data,
     }
    }
   });

fastify.register(require('fastify-mariadb'), {
    host: 'localhost',
    user: 'root',
    database: 'konsultanku_db',
    password:'',
    connectionLimit: 5,
  });
   
  fastify.get('/user/:id', (req, reply) => {

    fastify.mariadb.getConnection((err, conn) => {
      if (err) return reply.send(err);
      conn.query('SELECT id, email FROM users WHERE id=?', [req.params.id], (err, result) => {
        conn.release();
        reply.send(err || result);
      });
    });
  });
  fastify.get('/users', (req, reply) => {

    fastify.mariadb.getConnection((err, conn) => {
      if (err) return reply.send(err);
      conn.query('SELECT id, email FROM users', (err, result) => {
        conn.release();
        reply.send(err || result);
      });
    });
  });

  fastify.post('/users', async (req, reply) => {
      let email = req.body.email;
      let password = req.body.password;
      
      fastify.mariadb.getConnection((err, conn) => {
        if (err) return reply.send(err);
        conn.query(`INSERT INTO users(email, password)
        values(?, ?)`, [req.body.email], [req.body.password], (err, result) => {
          conn.release();
    reply.send(err || result);
        });
    });
});
  
   
  fastify.get('/mariadb/time', (req, reply) => {
    // `pool.query`
    fastify.mariadb.query('SELECT now()', (err, result) => {
      reply.send(err || result)
    });
  });

//Fungsi file root secara async.
const start = async () => {
  try {
    //Gunakan Port dari ENV APP_PORT, kalo ngga ada variable tersebut maka akan menggunakan port 3000
    await fastify.listen(3000)
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
//Jalankan server!
start()